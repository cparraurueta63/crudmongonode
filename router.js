const express = require('express');
const router = express.Router();
//controladores
const productoController = require('./src/controllers/productoController');

//rutas
router.get('/productos', productoController.mostrar);
router.post('/productos', productoController.crear);
router.put('/productos/:id', productoController.actualizar);
router.delete('/productos/:id', productoController.eliminar);

module.exports = router;