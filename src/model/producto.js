const conexion = require('../database/conexion');

const productosSchema = conexion.mongoose.Schema({
    nombre:String,
    cantidad:Number
}, {versionKey: false })

const ProductolModel = conexion.mongoose.model('productos', productosSchema)
module.exports = {
    ProductolModel,    
}