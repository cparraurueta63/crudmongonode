const express = require('express');
const routes = require('../router');
const app = express();
const port = process.env.Port || 8005; 
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.listen(port, () => console.log('El servidor escucha en el puerto ', port));
app.use('/', routes);