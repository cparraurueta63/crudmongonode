const { ProductolModel } = require("../model/producto");
//Mostrar los productos
const mostrar = async (request, response, next) => {
  try {
    const productos = await ProductolModel.find();

    if (productos.length === 0) {
      return response
        .status(404)
        .json({ mensaje: "No se encontraron productos." });
    }

    response.status(200).json(productos);
  } catch (error) {
    console.error("Error al recuperar productos:", error);
    response.status(500).json({ error: "Error interno del servidor." });
  }
};

//Crear
const crear = async (request, response, next) => {
  try {
    const { nombre, cantidad } = request.body;
    const errores = {};

    // Validaciones
    if (!nombre) {
      errores.nombre = "El campo 'nombre' es obligatorio.";
    } else if (typeof nombre !== "string") {
      errores.nombre = "El campo 'nombre' debe ser una cadena de texto.";
    } else if (nombre.length > 50) {
      errores.nombre = "El campo 'nombre' no puede tener más de 50 caracteres.";
    }

    if (!cantidad) {
      errores.cantidad = "El campo 'cantidad' es obligatorio.";
    } else if (typeof cantidad !== "number") {
      errores.cantidad = "El campo 'cantidad' debe ser un número.";
    }

    // Verificar si hay errores
    if (Object.keys(errores).length > 0) {
      return response.status(400).json({ errores });
    }

    // Si no hay errores, crear el producto

    const producto = new ProductolModel({
      nombre: request.body.nombre,
      cantidad: request.body.cantidad,
    });
    const resultado = await producto.save();
    response.status(200).json(resultado);
  } catch (error) {
    response.status(500).json(error);
  }
};
//editar

const actualizar = async (request, response, next) => {
  try {
    const { id } = request.params;
    const { nombre, cantidad } = request.body;
    const errores = {};

    // Validaciones
    if (nombre && typeof nombre !== "string") {
      errores.nombre = "El campo 'nombre' debe ser una cadena de texto.";
    } else if (nombre && nombre.length > 50) {
      errores.nombre = "El campo 'nombre' no puede tener más de 50 caracteres.";
    }

    if (cantidad && typeof cantidad !== "number") {
      errores.cantidad = "El campo 'cantidad' debe ser un número.";
    }

    // Verificar si hay errores
    if (Object.keys(errores).length > 0) {
      return response.status(400).json({ errores });
    }

    // Si no hay errores, realizar la actualización
    const estado = await ProductolModel.updateOne(
      { _id: id },
      {
        $set: {
          nombre,
          cantidad,
        },
      }
    );

    response
      .status(200)
      .json(`¿El producto ha sido actualizado? : ${estado.acknowledged}`);
  } catch (error) {
    response.status(500).json(error);
  }
};

const eliminar = async (request, response, next) => {
  try {
    const { id } = request.params;
    const resultado = await ProductolModel.deleteOne({ _id: id });

    if (resultado.deletedCount === 0) {
      // Si deletedCount es 0, significa que no se encontró el producto para eliminar
      return response
        .status(404)
        .json({ mensaje: "No se encontró el producto para eliminar." });
    }

    response
      .status(200)
      .json({ mensaje: "El producto ha sido eliminado con éxito." });
  } catch (error) {
    console.error("Error al eliminar el producto:", error);
    response.status(500).json({ error: "Error interno del servidor." });
  }
};

module.exports = {
  mostrar,
  crear,
  actualizar,
  eliminar,
};
