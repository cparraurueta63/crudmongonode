#Operaciones CRUD con Express.js y Mongoose

Este proyecto implementa las operaciones CRUD (Crear, Leer, Actualizar, Eliminar) para el modelo Producto utilizando Express.js y Mongoose. Se han creado rutas para agregar un producto, obtener todos los productos, actualizar un producto por su ID y eliminar un producto por su ID.

##Configuración del Proyecto

###Instalación de Dependencias

Asegúrate de tener Node.js y npm instalados. Luego, ejecuta el siguiente comando para instalar las dependencias:

npm install

###Configuración de la Base de Datos

Configura tu conexión a la base de datos MongoDB en el archivo config/database.js.

###Ejecución del Proyecto

Para iniciar el servidor, ejecuta:

npm start

#Rutas

Agregar un Producto

- URL: POST /productos
- Body JSON:
  {
    "nombre": "Nombre del Producto",
    "cantidad": 5
  }

Obtener Todos los Productos

- URL: GET /productos

Actualizar un Producto por su ID

- URL: PUT /productos/:id
- Body JSON:
  {
    "nombre": "Nuevo Nombre",
    "cantidad": 10
  }

Eliminar un Producto por su ID

- URL: DELETE /productos/:id

Ejemplo de JSON para Inserción

{
  "nombre": "Ejemplo Producto",
  "cantidad": 3
}


